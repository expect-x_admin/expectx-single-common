package net.expectx.common.single.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 统一返回结果类
 *
 * @author 李剑
 * @date 2018/5/21
 */
@ApiModel(value = "基本结果类型")
public class BaseResult implements Serializable {

    /**
     * 是否成功
     */
    @ApiModelProperty(value = "是否成功")
    private boolean success;

    /**
     * 状态码：200成功，其他为失败
     */
    @ApiModelProperty(value = "状态码:200成功，其他为失败")
    private int code;

    /**
     * 成功为success，其他为失败原因
     */
    @ApiModelProperty(value = "失败原因")
    private String message;

    /**
     * 数据结果集
     */
    @ApiModelProperty(value = "数据结果集")
    private Object data;

    public BaseResult(int code, String message, Object data) {
        if (code==BaseConstants.SUCCESS){
            this.success=true;
        }
        this.code = code;
        this.message = message;
        this.data = data;
    }
    public BaseResult(boolean success, String message, Object data) {
        if (success){
            this.code=BaseConstants.SUCCESS;
        }
        this.success=success;
        this.message = message;
        this.data = data;

    }
    public BaseResult(boolean success,int code, String message, Object data) {
        this.success=success;
        this.code = code;
        this.message = message;
        this.data = data;
    }
    public BaseResult(Boolean success,BaseResultConstants baseResultConstants, Object data) {
        this.success=success;
        this.code=baseResultConstants.getCode();
        this.message=baseResultConstants.getMessage();
        this.data=data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {

        return code;

    }

    public void setCode(int code) {

        this.code = code;

    }

    public String getMessage() {

        return message;

    }

    public void setMessage(String message) {

        this.message = message;

    }

    public Object getData() {

        return data;

    }

    public void setData(Object data) {

        this.data = data;

    }

    @Override
    public String toString() {
        return "BaseResult{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
