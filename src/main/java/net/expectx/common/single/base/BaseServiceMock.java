package net.expectx.common.single.base;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 降级实现BaseService抽象类
 * @author lijian
 */
public abstract class BaseServiceMock< Record, Example> implements BaseService<Record,Example> {
    @Autowired
    private BaseMapper baseMapper;

    @Override
    public BaseResult insert(Record record) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult insertSelective(Record record) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult deleteByExample(Example example) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult deleteByPrimaryKey(Object id) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult updateByExampleSelective(Record record, Example example) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult updateByExample(Record record, Example example) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(Record record) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public BaseResult updateByPrimaryKey(Record record) {
        return new BaseResult(false,"失败",null);
    }

    @Override
    public List<Record> selectByExample(Example example) {
        return null;
    }

    @Override
    public Record selectByPrimaryKey(Object id) {
        return null;
    }

    @Override
    public long countByExample(Example example) {
        return -1;
    }

    @Override
    public BaseResult physicalRemoveByExample(Byte isDeleted, Example example) {
        return null;
    }
}
