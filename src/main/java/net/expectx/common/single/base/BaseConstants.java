package net.expectx.common.single.base;

/**
 * 全局常量
 * @author jianhun
 * @date 2018/5/17
 */
public class BaseConstants {
    public static final String BASIC_COMMON_PROJECT_NAME="ExpectX";

    /**
     * 是否常量（0：否）
     */
    public static final byte NO = 0;

    /**
     * 是否常量（1：是）
     */
    public static final byte YES = 1;

    /**
     * 常量成功
     */
    public static final Integer SUCCESS=200;

    /**
     * 常量失败
     */
    public static final Integer FAILE=0;

    /**
     * http默认端口（默认值：80）
     */
    public static final int HTTP_PORT_DEFAULT = 80;

    /**
     * https默认端口（默认值：443）
     */
    public static final int HTTPS_PORT_DEFAULT = 443;

    /**
     * http请求类型：GET
     */
    public static final String HTTP_REQUEST_TYPE_GET = "GET";

    /**
     * http请求类型：POST
     */
    public static final String HTTP_REQUEST_TYPE_POST = "POST";

    /**
     * http网络协议
     */
    public static final String NETWORKING_PROTOCOL_HTTP="http";

    /**
     * https网络协议
     */
    public static final String NETWORKING_PROTOCOL_HTTPS="https";

    /**
     * 字符串unknown
     */
    public static final String STRING_UNKNOWN="unknown";

    /**
     * 存放AppKey的Request Key
     */
    public static final String REDIS_APPKEY_PREFIX = "AUTHORIZATION_APP_KEY_";

    public static final String DEFAULT_FILE_ROOT="Expect-X-Test";


    /**
     * 默认语言
     */
    public static final String DEFAULT_LANGUAGE = "zh";

    public static final Integer STATUS_OK=200;

    public static final String BASIC_IDENTIFICATION_SUCCESS="success";
    public static final String BASIC_IDENTIFICATION_CODE="code";
    public static final String BASIC_IDENTIFICATION_MESSAGE="message";
    public static final String BASIC_IDENTIFICATION_DATA="data";
    public static final String BASIC_IDENTIFICATION_RECORD="record";
}
