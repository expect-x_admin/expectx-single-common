package net.expectx.common.single.base;

import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 *
 * @param <Record>
 * @param <Example>
 * @author lj
 * 2019-06-16
 */
public interface BaseMapper<Record, Example> {
    /**
     * 插入记录
     * @param record
     * @return
     */
    int insert(Record record);

    /**
     * 插入记录有效字段
     * @param record
     * @return
     */
    int insertSelective(Record record);

    /**
     * 根据条件删除记录
     * @param example
     * @return
     */
    int deleteByExample(Example example);

    /**
     * 根据主键删除记录
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Object id);

    /**
     * 根据条件更新有效字段
     * @param record
     * @param example
     * @return
     */
    int updateByExampleSelective(@Param("record") Record record, @Param("example") Example example);

    /**
     * 根据条件更新记录
     * @param record
     * @param example
     * @return
     */
    int updateByExample(@Param("record") Record record, @Param("example") Example example);

    /**
     * 根据主键更新记录有效字段
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Record record);

    /**
     * 根据主键更新记录
     * @param record
     * @return
     */
    int updateByPrimaryKey(Record record);


    /**
     * 根据条件查询记录
     * @param example
     * @return
     */
    List<Record> selectByExample(Example example);

    /**
     * 根据主键查询记录
     * @param id
     * @return
     */
    Record selectByPrimaryKey(Object id);


    /**
     * 根据条件查询记录数量
     * @param example
     * @return
     */
    long countByExample(Example example);


    /**
     * 根据条件物理启用或删除记录
     * @param isDeleted
     * @param example
     * @return
     */
    int physicalRemoveByExample(@Param("isDeleted") Byte isDeleted, @Param("example") Example example );




}
