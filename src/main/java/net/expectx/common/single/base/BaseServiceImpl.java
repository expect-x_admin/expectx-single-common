package net.expectx.common.single.base;

import net.expectx.common.single.db.DataSourceEnum;
import net.expectx.common.single.db.DynamicDataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author lijian
 */
public abstract class BaseServiceImpl<Record, Example> implements BaseService<Record, Example> {
    private static final Logger LOGGER = Logger.getLogger(BaseServiceImpl.class);

    @Autowired
    private BaseMapper<Record, Example> baseMapper;

    @Override
    public BaseResult insert(Record record) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.insert(record) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_INSERT,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }

    }

    @Override
    public BaseResult insertSelective(Record record) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.insertSelective(record) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_INSERT,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }

    @Override
    public BaseResult deleteByExample(Example example) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            baseMapper.deleteByExample(example);
            DynamicDataSource.clearDataSource();
            return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,null);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }

    @Override
    public BaseResult deleteByPrimaryKey(Object id) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            baseMapper.deleteByPrimaryKey(id);
            DynamicDataSource.clearDataSource();
            return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,null);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }

    @Override
    public BaseResult updateByExampleSelective(Record record, Example example) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.updateByExampleSelective(record, example) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_UPDATE,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }


    @Override
    public BaseResult updateByExample(Record record, Example example) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.updateByExample(record, example) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_UPDATE,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(Record record) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.updateByPrimaryKeySelective(record) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_UPDATE,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }

    }


    @Override
    public BaseResult updateByPrimaryKey(Record record) {
        try {
            DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
            boolean flag = baseMapper.updateByPrimaryKey(record) > 0 ? true : false;
            DynamicDataSource.clearDataSource();
            if (!flag) {
                return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_UPDATE,null);
            }else{
                return new BaseResult(Boolean.TRUE,BaseResultConstants.SUCCESS,record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("系统错误:"+e.getLocalizedMessage());
            return new BaseResult(Boolean.FALSE, BaseConstants.FAILE, "系统错误:"+e.getLocalizedMessage(), null);
        }
    }


    @Override
    public List<Record> selectByExample(Example example) {
        DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
        List<Record> records = baseMapper.selectByExample(example);
        DynamicDataSource.clearDataSource();
        return records;
    }


    @Override
    public Record selectByPrimaryKey(Object id) {
        DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
        Record record = baseMapper.selectByPrimaryKey(id);
        DynamicDataSource.clearDataSource();
        return record;
    }

    @Override
    public long countByExample(Example example) {
        DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
        long count = baseMapper.countByExample(example);
        DynamicDataSource.clearDataSource();
        return count;
    }

    @Override
    public BaseResult physicalRemoveByExample(Byte isDeleted, Example example) {
        DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
        int i = baseMapper.physicalRemoveByExample(isDeleted, example);
        DynamicDataSource.clearDataSource();
        if (i == 0) {
            LOGGER.error("删除成功");

            return new BaseResult(Boolean.FALSE,BaseResultConstants.EXCEPTION_DATA_BASE_DELETE, null);
        } else {
            return new BaseResult( Boolean.TRUE,BaseResultConstants.SUCCESS, null);
        }
    }


}
