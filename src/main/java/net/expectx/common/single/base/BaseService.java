package net.expectx.common.single.base;




import java.util.List;

/**
 * @author  lj
 * 2018-05-17
 * @param
 */
public interface BaseService<Record,Example> {

    /**
     * 插入记录
     * @param record
     * @return
     */
    BaseResult insert(Record record);

    /**
     * 插入记录有效字段
     * @param record
     * @return
     */
    BaseResult insertSelective(Record record);

    /**
     * 根据条件删除记录
     * @param example
     * @return
     */
    BaseResult deleteByExample(Example example);

    /**
     * 根据主键删除记录
     * @param id
     * @return
     */
    BaseResult deleteByPrimaryKey(Object id);

    /**
     * 根据条件更新有效字段
     * @param record
     * @param example
     * @return
     */
    BaseResult updateByExampleSelective( Record record,Example example);

    /**
     * 根据条件更新记录
     * @param record
     * @param example
     * @return
     */
    BaseResult updateByExample( Record record, Example example);

    /**
     * 根据主键更新记录有效字段
     * @param record
     * @return
     */
    BaseResult updateByPrimaryKeySelective(Record record);


    /**
     * 根据主键更新记录
     * @param record
     * @return
     */
    BaseResult updateByPrimaryKey(Record record);


    /**
     * 根据条件查询记录
     * @param example
     * @return
     */
    List<Record> selectByExample(Example example);


    /**
     * 根据主键查询记录
     * @param id
     * @return
     */
    Record selectByPrimaryKey(Object id);

    /**
     * 根据条件查询记录数量
     * @param example
     * @return
     */
    long countByExample(Example example);

    /**
     * 根据条件物理启用或删除记录
     * @param isDeleted
     * @param example
     * @return
     */
    BaseResult physicalRemoveByExample(Byte isDeleted,Example example );
}
