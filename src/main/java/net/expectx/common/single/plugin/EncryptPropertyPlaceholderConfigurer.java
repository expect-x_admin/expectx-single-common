package net.expectx.common.single.plugin;

import net.expectx.common.single.util.AesUtil;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * 支持加密配置文件插件
 * @author jianhun
 * @date 2018/5/22
 */
public class EncryptPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	private String[] propertyNames = {
		"master.jdbc.password", "slave.jdbc.password", "generator.jdbc.password", "master.redis.password"
	};

	/**
	 * 解密指定propertyName的加密属性值
	 * @param propertyName
	 * @param propertyValue
	 * @return
	 */
	@Override
	protected String convertProperty(String propertyName, String propertyValue) {

		for (String p : propertyNames) {
			if (p.equalsIgnoreCase(propertyName)) {
				System.out.println(propertyValue);
				return AesUtil.aesDecode(propertyValue);
			}
		}
		return super.convertProperty(propertyName, propertyValue);

	}

}
