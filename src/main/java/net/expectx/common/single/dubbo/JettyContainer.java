package net.expectx.common.single.dubbo;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.dubbo.common.utils.ConfigUtils;
import com.alibaba.dubbo.common.utils.NetUtils;
import com.alibaba.dubbo.container.Container;
import com.alibaba.dubbo.container.page.PageServlet;
import com.alibaba.dubbo.container.page.ResourceFilter;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.servlet.FilterHolder;
import org.mortbay.jetty.servlet.ServletHandler;
import org.mortbay.jetty.servlet.ServletHolder;

/**
 * @author lijian
 * dubbo jetty服务器
 */
public class JettyContainer implements Container {
    private static final Logger logger = LoggerFactory.getLogger(com.alibaba.dubbo.container.jetty.JettyContainer.class);
    public static final Integer JETTY_PORT = PropertiesFileUtil.getInstance("dubbo").getInt("dubbo.jetty.port");
    public static final String JETTY_DIRECTORY = "dubbo.jetty.directory";
    public static final String JETTY_PAGES = "dubbo.jetty.page";
    public static final int DEFAULT_JETTY_PORT = 9999;
    SelectChannelConnector connector;

    public JettyContainer() {
    }
    @Override
    public void start() {
        int port;
        if (JETTY_PORT != null && JETTY_PORT != 0) {
            port = JETTY_PORT;
        } else {
            port = 8080;
        }

        this.connector = new SelectChannelConnector();
        this.connector.setPort(port);
        ServletHandler handler = new ServletHandler();
        String resources = ConfigUtils.getProperty("dubbo.jetty.directory");
        if (resources != null && resources.length() > 0) {
            FilterHolder resourceHolder = handler.addFilterWithMapping(ResourceFilter.class, "/*", 0);
            resourceHolder.setInitParameter("resources", resources);
        }

        ServletHolder pageHolder = handler.addServletWithMapping(PageServlet.class, "/*");
        pageHolder.setInitParameter("pages", ConfigUtils.getProperty("dubbo.jetty.page"));
        pageHolder.setInitOrder(2);

        handler.addServletWithMapping(StatViewServlet.class, "/druid/*");

        Server server = new Server();
        server.addConnector(this.connector);
        server.addHandler(handler);

        try {
            server.start();
        } catch (Exception var8) {
            throw new IllegalStateException("Failed to start jetty server on " + NetUtils.getLocalHost() + ":" + port + ", cause: " + var8.getMessage(), var8);
        }
    }
    @Override
    public void stop() {
        try {
            if (this.connector != null) {
                this.connector.close();
                this.connector = null;
            }
        } catch (Throwable var2) {
            logger.error(var2.getMessage(), var2);
        }

    }
}
