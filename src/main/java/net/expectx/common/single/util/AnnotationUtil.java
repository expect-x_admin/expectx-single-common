package net.expectx.common.single.util;

import com.google.common.collect.Maps;
import io.swagger.annotations.Api;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @author lijian
 * 注解工具类
 */
public class AnnotationUtil {

    public static void validAnnotation(List<Class<?>> clsList){
        if (clsList != null && clsList.size() > 0) {
            for (Class<?> cls : clsList) {
                //获取类中的所有的方法
                Method[] methods = cls.getDeclaredMethods();
                System.out.println(methods[0]);
                if (methods != null && methods.length > 0) {
                    for (Method method : methods) {
                        Api api = (Api) method.getAnnotation(Api.class);
                        System.out.println(api);
                    }
                }
            }
        }
    }

    /**
     * 获得类注释
     * @param cls
     * @return
     */
    public static String getClassAnnotation(Class<?> cls){
            Api api = (Api) cls.getAnnotation(Api.class);
            return api.value();
    }
    /**
     * 获得类注释
     * @param clsList
     * @return
     */
    public static Map<String,String> getClassAnnotationes(List<Class<?>> clsList){
        Map<String, String> classMap = Maps.newHashMapWithExpectedSize(7);
        for (Class<?> cls : clsList) {
            classMap.put(cls.getSimpleName(), getClassAnnotation(cls));
            //获取类中的所有的方法
            Method[] methods = cls.getDeclaredMethods();
        }
        return classMap;
    }


}
