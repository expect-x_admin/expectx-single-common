package net.expectx.common.single.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lijian
 */
public class ValidatorUtil {
    static final Pattern MOBILE_REGULAY = Pattern.compile("^1[3|4|5|7|8][0-9]\\d{4,8}$");

    /**
     * 验证手机
     * @param mobile
     * @return
     */
    public static boolean validatorMobile(String mobile){
        Matcher m = MOBILE_REGULAY.matcher(mobile);
        return m.matches();
    }
}
