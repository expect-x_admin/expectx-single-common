package net.expectx.common.single.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.io.UnsupportedEncodingException;

/**
 * 中文转拼音工具类
 * @author lijian
 */
public class ChineseCharToEn {
	private final static int[] LI_SECPOSVALUE = { 1601, 1637, 1833, 2078, 2274,
			2302, 2433, 2594, 2787, 3106, 3212, 3472, 3635, 3722, 3730, 3858,
			4027, 4086, 4390, 4558, 4684, 4925, 5249, 5590 };
	private final static String[] LC_FIRSTLETTER = { "a", "b", "c", "d", "e",
			"f", "g", "h", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
			"t", "w", "x", "y", "z" };
	private final static Integer A=1600;
    private final static Integer B=5590;
    private final static Integer C=23;
	/** */
	/**
	 * 取得给定汉字串的首字母串,即声母串
	 *
	 * @param str
	 *            给定汉字串
	 * @return 声母串
	 */
	public static String getAllFirstLetter(String str) {
		if (str == null || str.trim().length() == 0) {
			return "";
		}

		String temp = "";

		for (int i = 0; i < str.length(); i++) {
            temp =temp + getFirstLetter(str.substring(i, i + 1));
		}
		return temp;
	}

	/** */
	/**
	 * 取得给定汉字的首字母,即声母
	 *
	 * @param chinese
	 *            给定的汉字
	 * @return 给定汉字的声母
	 */
	public static String getFirstLetter(String chinese) {
		if (chinese == null || chinese.trim().length() == 0) {
			return "";
		}
		chinese = conversionStr(chinese, "GBK", "ISO8859-1");
		if (chinese.length() > 1)
		{
			int liSectorCode = (int) chinese.charAt(0);
			int liPositionCode = (int) chinese.charAt(1);
			liSectorCode = liSectorCode - 160;
			liPositionCode = liPositionCode - 160;
			int liSecPosCode = liSectorCode * 100 + liPositionCode;
			if (liSecPosCode > A && liSecPosCode < B) {
				for (int i = 0; i <C; i++) {
					if (liSecPosCode >= LI_SECPOSVALUE[i]
							&& liSecPosCode < LI_SECPOSVALUE[i + 1]) {
						chinese = LC_FIRSTLETTER[i];
						break;
					}
				}
			} else //  非汉字字符,如图形符号或ASCII码
			{
				chinese = conversionStr(chinese, "ISO8859-1", "GBK");
				chinese = chinese.substring(0, 1);
			}
		}

		return chinese;
	}
	/**
	 * 取得给定短语的各字首字母
	 * @param str
	 * @return
	 */
	public static String getAllFirstLetters(String str) {
		String convert = "";
		for (int j = 0; j < str.length(); j++) {
			char word = str.charAt(j);
			String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
			if (pinyinArray != null) {
				convert += pinyinArray[0].charAt(0);
			} else {
				convert += word;
			}
		}
		return convert;
	}

	private static String conversionStr(String str, String charsetName,
			String toCharsetName) {
		try {
			str = new String(str.getBytes(charsetName), toCharsetName);

		} catch (UnsupportedEncodingException ex) {
			System.out.println("字符串编码转换异常：" + ex.getMessage());
		}

		return str;
	}

	 public static  String getFirstSpell(String chinese) {
         StringBuffer pybf = new StringBuffer();
         char[] arr = chinese.toCharArray();
         HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
         defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
         defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
         for (int i = 0; i < arr.length; i++) {
                 if (arr[i] > 128) {
                         try {
                                 String[] temp = PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat);
                                 if (temp != null) {
                                         pybf.append(temp[0].charAt(0));
                                 }
                         } catch (BadHanyuPinyinOutputFormatCombination e) {
                                 e.printStackTrace();
                         }
                 } else {
                         pybf.append(arr[i]);
                 }
         }
         return pybf.toString().replaceAll("\\W", "").trim();
 }
	public static boolean containsAny(String str, String searchChars) {
		  if(str.length()!=str.replace(searchChars,"").length())  {
		   return true;
		  }
		  return false;
   }

}
