package net.expectx.common.single.util;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * 经纬度工具类
 * @author Jianhun
 * @date 2018/5/21
 */
public class DistributionUtil {

    /**
     * 验证经度
     */
    private static Pattern ISVALIDOFLNG =
            Pattern.compile("^[\\-\\+]?(0?\\d{1,2}\\.\\d{1,20}|1[0-7]?\\d{1}\\.\\d{1,20}|180\\.0{1,20})$");

    /**
     * 验证维度
     */
    private static Pattern ISVALIDOFLAT =
            Pattern.compile("^[\\-\\+]?([0-8]?\\d{1}\\.\\d{1,20}|90\\.0{1,20})$");

    /**
     * 计算两点之间距离（与高德计算的误差为1.76m）
     * @param startLng
     * @param startLat
     * @param endLng
     * @param endLat
     * @return
     */
    public static String getDistance(String startLng, String startLat, String endLng, String endLat) {

        double lng1 = (Math.PI / 180) * Double.parseDouble(startLng);
        double lng2 = (Math.PI / 180) * Double.parseDouble(endLng);
        double lat1 = (Math.PI / 180) * Double.parseDouble(startLat);
        double lat2 = (Math.PI / 180) * Double.parseDouble(endLat);

        // 地球半径(单位：千米)
        double earthRadius = 6371;

        // 两点间距离（单位：米）
        double d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1))
                * earthRadius;
        return new DecimalFormat("#").format(d * 1000);
    }

    /**
     * 验证经度是否合法
     * @param lng
     * @return
     */
    public static boolean isValidOfLng(String lng) {

        boolean boo = false;
        if(StringUtils.isNotBlank(lng)) {
            boo = ISVALIDOFLNG.matcher(lng).matches();
        }

        return boo;

    }

    /**
     * 验证维度是否合法
     * @param lat
     * @return
     */
    public static boolean isValidOfLat(String lat) {

        boolean boo = false;
        if(StringUtils.isNotBlank(lat)) {
            boo = ISVALIDOFLAT.matcher(lat).matches();
        }
        return boo;
    }

}
