package net.expectx.common.single.util;

import java.util.regex.Pattern;

/**
 * 手机、固话号码验证工具类
 * @author jianhun
 * @date 2018/5/21
 */
public class PhoneUtil {

    /**
     * 验证手机号
     */
    private static Pattern IS_MOBILE = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$");

    /**
     * 验证带区号的
     */
    private static Pattern IS_PHONE_WITH_AREA_CODE = Pattern.compile("^[0][1-9]{2,3}-[0-9]{5,10}$");

    /**
     * 验证没有区号的
     */
    private static Pattern IS_PHONE = Pattern.compile("^[1-9]{1}[0-9]{5,8}$");

    /**
     * 验证IMEI
     */
    private static Pattern IS_IMEI = Pattern.compile("^\\d{14,15}$");

    /**
     * 验证IMSI
     */
    private static Pattern IS_IMSI = Pattern.compile("^(460)(00|01|02|03|07)(\\w{10})$");

    /**
     * 手机号验证
     * @param  str
     * @return 验证通过返回true
     */
    public static boolean isMobile(String str) {

        boolean b = false;
        b = IS_MOBILE.matcher(str).matches();
        return b;

    }
    /**
     * 电话号码验证
     * @param  str
     * @return 验证通过返回true
     */
    public static boolean isPhone(String str) {

        boolean b = false;
        final int phoneLength = 9;
        if(str.length() > phoneLength)
        {
            b = IS_PHONE_WITH_AREA_CODE.matcher(str).matches();
        }else{
            b = IS_PHONE.matcher(str).matches();
        }
        return b;

    }

    /**
     * 国际移动装备辨识码验证
     * @param str
     * @return
     */
    public static boolean isImei(String str) {

        boolean b = false;
        b = IS_IMEI.matcher(str).matches();
        return b;

    }

    /**
     * 国际移动用户识别码验证
     * @param str
     * @return
     */
    public static boolean isImsi(String str) {

        boolean b = false;
        b = IS_IMSI.matcher(str).matches();
        return b;

    }

}
