package net.expectx.common.single.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @desc:SHA1工具
 * @author lj
 *
 */
public class Sha1Util {
	public static String sha1Encode(String sourceString)
			throws NoSuchAlgorithmException {
		String resultString = null;
		resultString = new String(sourceString);
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		resultString = byte2hexString(md.digest(resultString.getBytes()));
		return resultString;
	}

	public static final String byte2hexString(byte[] bytes) {
		StringBuffer buf = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			if (((int) bytes[i] & 0xff) < 0x10) {
				buf.append("0");
			}
			buf.append(Long.toString((int) bytes[i] & 0xff, 16));
		}
		return buf.toString().toLowerCase();
	}

}
