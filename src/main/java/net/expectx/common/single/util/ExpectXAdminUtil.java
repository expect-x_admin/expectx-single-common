package net.expectx.common.single.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * 启动解压ChishipAdmin-x.x.x.jar到resources目录
 *
 * @author lijian
 * @date 2018/6/6
 */
public class ExpectXAdminUtil implements InitializingBean, ServletContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpectXAdminUtil.class);
    /**
     * 模块名 eg.chiship-admin-h+
     */
    private String module;
    /**
     * 版本 eg.1.0.0
     */
    private String version;


    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void setServletContext(ServletContext servletContext) {

        LOGGER.info("===== 开始解压vhiship-admin =====");
        String version = getVersion();
        LOGGER.info(module+".jar 版本: {}", version);
        String jarPath = servletContext.getRealPath("/WEB-INF/lib/"+module+"-" + version + ".jar");
        LOGGER.info(module+".jar 包路径: {}", jarPath);
        String resources = servletContext.getRealPath("/") + "/resources/"+module;
        LOGGER.info(module+".jar 解压到: {}", resources);
        JarUtil.decompress(jarPath, resources);
        LOGGER.info("===== 解压"+module+"完成 =====");

    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
