package net.expectx.common.single.util;

import org.apache.commons.lang.StringUtils;

import java.util.Random;
import java.util.regex.Pattern;

/**
 * 令牌工具类
 * @author jianhun
 * @date 2018/5/21
 */
public class TokenUtil {

    /**
     * 验证令牌是否合法
     */
    private static Pattern IS_VALID = Pattern.compile("^[0-9a-z]{36}$");

    /**
     * 用户令牌（随机字符）
     * @return
     */
    public static String getRandomToken() {

        int pwdLen = 36;
        // 35是因为数组是从0开始的，26个字母+10个数字
        final int maxNum = 36;
        // 生成的随机数
        int i;
        // 生成的密码的长度
        int count = 0;
        char[] str = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < pwdLen) {
            // 生成随机数，取绝对值，防止生成负数，生成的数最大为36-1
            i = Math.abs(r.nextInt(maxNum));
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();

    }

    /**
     * 用户令牌是否合法
     * @param accessToken
     * @return
     */
    public static boolean isValid(String accessToken){

        boolean boo = false;
        if(StringUtils.isNotBlank(accessToken)) {
            boo = IS_VALID.matcher(accessToken).matches();
        }

        return boo;

    }

}
