package net.expectx.common.single.util;

import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseConstants;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.base.BaseResultConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 */
public class CommonUtil {
    /**
     * 过滤语言
     * @param content
     * @param language
     * @return
     */
    public static String filterLanguage(String content,String language){

        try {
            if (null==language){
                language= BaseConstants.DEFAULT_LANGUAGE;
            }
            JSONObject json=JSONObject.parseObject(content);
            return json.getString(language);
        }catch (Exception e){
            return content;
        }
    }

    /**
     * 校验密码是否符合规范
     * @param password
     * @return
     */
    public static BaseResult validPassword(String password){
        if (password.length() < 8 || password.length()> 20) {
            return new BaseResult(Boolean.FALSE,BaseConstants.FAILE,"请填写8-20个字符的密码",null);
        }
        if (!hasNumber(password)) {
            return new BaseResult(Boolean.FALSE,BaseConstants.FAILE,"密码必须含有数字",null);
        }

        if (!hasSmallChar(password)) {
            return new BaseResult(Boolean.FALSE,BaseConstants.FAILE,"密码必须含有小写字母",null);
        }
        if (!hasBigChar(password)) {
            return new BaseResult(Boolean.FALSE,BaseConstants.FAILE,"密码必须含有大写字母",null);
        }
        return new BaseResult(Boolean.TRUE, BaseResultConstants.SUCCESS,null);

    }
    private static  boolean hasNumber(String value){
        boolean flag=false;
        String letters = "1234567890";
        for(int i=0;i<value.length();i++) {
            char c = value.charAt(i);
            if (letters.indexOf(c) != -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    private static  boolean hasSmallChar(String value){
        boolean flag=false;
        String letters = "abcdefghijklnmopqrstuvwxyz";
        for(int i=0;i<value.length();i++) {
            char c = value.charAt(i);
            if (letters.indexOf(c) != -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    private static  boolean hasBigChar(String value){
        boolean flag=false;
        String letters = "ABCDEFGHIJKLNMOPQRSTUVWXYZ";
        for(int i=0;i<value.length();i++) {
            char c = value.charAt(i);
            if (letters.indexOf(c) != -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    /**
     * 允许上传文件的扩展后缀
     * @return
     */
    public static Map allowUploadFileExt(){
        HashMap<String, String> extMap = new HashMap<String, String>(4);
        /*图片*/
        extMap.put("images", "png,jpg,jpeg");
        /*所有文件*/
        extMap.put("file", "png,jpg,jpeg,doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2,avi,mp4,pdf");
        /*音频*/
        extMap.put("audio", "mp3");
        /*视频*/
        extMap.put("video", "swf,flv,mp4,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
        return  extMap;
    }
}
