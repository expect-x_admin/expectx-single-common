package net.expectx.common.single.util;

import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * @author 李剑
 * 将图片转换为Base64<br>
 * 将base64编码字符串解码成img图片
 * @创建时间 2018-02-07
 *
 */
public class ImageUtil {


    /**
     * 将图片转换成Base64编码
     * @param imgFile 待处理图片
     * @return
     */
    public static String getImgStrByFile(String imgFile){
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return new String(Base64.encodeBase64(data));
    }

    /**
     * 网络图片转换base64
     * @param urlList
     * @return
     */
    public static String getImgStrByUrl(String urlList){
        String base64String = "";
        try {
            URL url = new URL(urlList);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream inStream = conn.getInputStream();
            byte[] data = readInputStream(inStream);
            base64String= new String(Base64.encodeBase64(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64String;
    }

    /**
     * 链接url下载图片
     * @param urlPath
     * @param savePath
     */
    public static String downloadPicture(String urlPath,String savePath) throws Exception{
        savePath=savePath+"//"+urlPath.substring(urlPath.lastIndexOf("/")+1);
        URL url = new URL(urlPath);
        DataInputStream dataInputStream = new DataInputStream(url.openStream());
        FileOutputStream fileOutputStream = new FileOutputStream(new File(savePath));
        fileOutputStream.write(readInputStream(dataInputStream));
        dataInputStream.close();
        fileOutputStream.close();
        return savePath;
    }
    /**
     * 网络图片转换Byte
     * @param urlList
     * @return
     */
    public static byte[] getImgByteByUrl(String urlList){
        try {
            URL url = new URL(urlList);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream inStream = conn.getInputStream();
            byte[] data = readInputStream(inStream);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    private static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    /**
     * 对字节数组字符串进行Base64解码并生成图片
     * @param imgStr 图片数据
     * @param imgFilePath 保存图片全路径地址
     * @return
     */
    public static boolean generateImage(String imgStr,String imgFilePath){
        if (imgStr == null){
            return false;
        }
        try {
            byte[] b = Base64.decodeBase64(imgStr);
            for(int i=0;i<b.length;++i) {
                if(b[i]<0){
                    b[i]+=256;
                }
            }
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public static void main(String [] args){
    }
}
