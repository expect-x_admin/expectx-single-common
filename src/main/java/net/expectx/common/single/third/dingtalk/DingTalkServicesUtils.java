package net.expectx.common.single.third.dingtalk;

import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.HttpUtils;
import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 钉钉服务工具类
 * @author lijian
 */
public class DingTalkServicesUtils {
    /**
     * 获得accessToken
     * @return
     */
    public static BaseResult getToken(){
        Map<String, String> query = new HashMap<>(2);
        query.put("appkey", DingTalkConstant.APP_KEY);
        query.put("appsecret", DingTalkConstant.APP_SECRET);
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_TOKEN, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 获取部门列表
     * @param accessToken   令牌
     * @return
     */
    public static BaseResult getDepartmentList(String accessToken){
        Map<String, String> query = new HashMap<>(2);
        query.put("access_token",accessToken);
        query.put("fetch_child",String.valueOf(Boolean.TRUE));
        query.put("id",String.valueOf(1));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_DEPARTMENT_LIST, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 获取部门用户
     * @param accessToken       令牌
     * @param departmentId      部门ID
     * @return
     */
    public static BaseResult getUserList(String accessToken,Long departmentId ){
        Map<String, String> query = new HashMap<>(2);
        query.put("access_token",accessToken);
        query.put("department_id",String.valueOf(departmentId));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_USER_LIST, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }
    /**
     * 获取部门用户详情
     * @param accessToken       令牌
     * @param departmentId      部门ID
     * @param page              当前页码
     * @return
     */
    public static BaseResult getUserListByPage(String accessToken,Long departmentId,Integer page ){
        Map<String, String> query = new HashMap<>(2);
        query.put("access_token",accessToken);
        query.put("department_id",String.valueOf(departmentId));
        query.put("offset",String.valueOf((page-1)*50));
        query.put("size",String.valueOf(50));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_USER_LIST_BY_PAGE, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 获取角色列表
     * @param accessToken       令牌
     * @return
     */
    public static BaseResult getRoleList(String accessToken){
        Map<String, String> query = new HashMap<>(2);
        query.put("access_token",accessToken);
        query.put("offset",String.valueOf(0));
        query.put("size",String.valueOf(50));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_ROLE_LIST, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 获取角色下的员工列表
     * @param accessToken       令牌
     * @param roleId            角色
     * @return
     */
    public static BaseResult getRoleUserList(String accessToken,Long roleId){
        Map<String, String> query = new HashMap<>(2);
        query.put("access_token",accessToken);
        query.put("role_id",String.valueOf(roleId));
        query.put("offset",String.valueOf(0));
        query.put("size",String.valueOf(50));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_ROLE_USER_LIST, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 获得日志
     * @param accessToken
     * @param templateName
     * @param day
     * @param userId
     * @return
     */
    public static BaseResult getReportList(String accessToken,String templateName,Integer day,String userId){
        Map<String, String> query = new HashMap<>(2);
        if (null==templateName){
            templateName="日报";
        }
        if (null==day){
            day=1;
        }
        query.put("access_token",accessToken);
        query.put("start_time",String.valueOf(System.currentTimeMillis()-day*24*60*60*1000));
        query.put("end_time",String.valueOf(System.currentTimeMillis()));
        query.put("template_name",templateName);
        if (null!=userId){
            query.put("userid",userId);
        }
        query.put("cursor",String.valueOf(0));
        query.put("size",String.valueOf(50));
        try{
            HttpResponse response = HttpUtils.doGet(DingTalkConstant.DING_TALK_SERVER_HOST, DingTalkConstant.GET_REPORT_LIST, DingTalkConstant.HTTP_REQUEST_TYPE_GET, DingTalkConstant.commonHeaders(),query);
            return DingTalkConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }




}
