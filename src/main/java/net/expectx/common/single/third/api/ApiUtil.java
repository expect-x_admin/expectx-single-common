package net.expectx.common.single.third.api;

import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.AesUtil;
import net.expectx.common.single.util.Md5Util;
import net.expectx.common.single.util.RedisUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.*;

/**
 * @author lijian
 */
public class ApiUtil {
    private static final String MD5="MD5";
    private static final String AES="AES";
    /**
     * 公共请求头
     * @return
     */
    public static Map<String,String>commonHeaders(){
        Map<String, String> headers = new HashMap<>(2);
        headers.put("MAC", "20180501");
        return headers;
    }

    public static JSONObject analysisHttpResponse( HttpResponse response) throws Exception{
        JSONObject json= JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
        return  json;
    }
    /**
     * 参数加密
     * @param params
     * @return
     * @throws Exception
     */
    public static String encryptionParam(Map<String,String> params) throws Exception{
        if (null== RedisUtil.get(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX)){
            BaseResult encryptionResult=ApiServicesUtils.systemEncryption();
            if (encryptionResult.isSuccess()){
                RedisUtil.set(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX,((JSONObject)encryptionResult.getData()).toJSONString());
            }else{
                RedisUtil.set(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX,"{\""+ApiConstant.BASIC_IDENTIFICATION_SUCCESS+"\":\"false\"}");
            }
        }else{
            try {
                JSONObject.parseObject(RedisUtil.get(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX));
            }catch (Exception e){
                RedisUtil.set(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX,"{\""+ApiConstant.BASIC_IDENTIFICATION_SUCCESS+"\":\"false\"}");
            }
        }
        JSONObject encryptionJSON= JSONObject.parseObject(RedisUtil.get(ApiConstant.REDIS_KEY_ENCRYPTION_PREFIX));
        Byte encryptionTime=1;
        String encryptionCode="ExpectX";
        String encryptionMethod="MD5";
        if (!encryptionJSON.containsKey(ApiConstant.BASIC_IDENTIFICATION_SUCCESS)){
            encryptionTime=encryptionJSON.getByte("encryptionTime");
            encryptionCode=encryptionJSON.getString("encryptionCode");
            encryptionMethod=encryptionJSON.getString("encryptionMethod");
        }
        /**
         * 获得参数并字典排序
         */
        List<String> parameterValueList=new LinkedList<>();
        for (Map.Entry<String,String> entry:params.entrySet()){
            parameterValueList.add(entry.getValue());
        }
        parameterValueList.add(encryptionCode);
        String [] parameterValues=new String[parameterValueList.size()];
        for(int i=0,j=parameterValueList.size();i<j;i++){
            parameterValues[i]=parameterValueList.get(i);
        }
        Arrays.sort(parameterValues);
        StringBuffer parameterValue=new StringBuffer();
        for (int i = 0; i < parameterValues.length; i++) {
            parameterValue.append(parameterValues[i]);
        }
        System.out.println("排序后字符串:"+parameterValue.toString());
        if(MD5.equals(encryptionMethod)){
            String tempEncrypt= Md5Util.md5(parameterValue.toString(),"UTF-8").toLowerCase();
            System.out.println("第1次加密:"+tempEncrypt);
            for (int i=0;i<encryptionTime-1;i++){
                tempEncrypt= Md5Util.md5(tempEncrypt).toLowerCase();
                System.out.println("第"+(i+2)+"次加密:"+tempEncrypt);
            }
            System.out.println("Encrypt String:"+tempEncrypt.toLowerCase());
            return tempEncrypt.toLowerCase();
        }else if(AES.equals(encryptionMethod)){
            String tempEncrypt= AesUtil.aesEncode(parameterValue.toString());
            for (int i=0;i<encryptionTime-1;i++){
                tempEncrypt= AesUtil.aesEncode(tempEncrypt);
            }

        }
        return null;
    }
}
