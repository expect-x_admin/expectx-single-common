package net.expectx.common.single.third.baidu;


import net.expectx.common.single.base.BaseResult;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 世界结果分析
 * @author lijian
 */
public class BaiDuAiResultAnalyze {
    public static BaseResult analyzeOCR(BaseResult thirdResult){
        Map map=new HashMap(2);
        boolean success=false;
        String message=null;
        try {
            if(thirdResult.isSuccess()){
                JSONObject json=(JSONObject) thirdResult.getData();
                JSONObject wordsResult=json.getJSONObject("words_result");
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_REAL_NAME)){
                    map.put("realName","");
                }else{
                    map.put("realName",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_REAL_NAME).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_REAL_NATION)){
                    map.put("nation","");
                }else{
                    map.put("nation",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_REAL_NATION).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_BIRTHDAY)){
                    map.put("birthday","");
                }else{
                    map.put("birthday",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_BIRTHDAY).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_ADDRESS)){
                    map.put("address","");
                }else{
                    map.put("address",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_ADDRESS).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_ID)){
                    map.put("ID","");
                }else{
                    map.put("ID",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_ID).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_SEX)){
                    map.put("sex","");
                }else{
                    map.put("sex",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_SEX).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_EXPIRATION_DATE)){
                    map.put("expirationDate","");
                }else{
                    map.put("expirationDate",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_EXPIRATION_DATE).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_ISSUE_OFFICE)){
                    map.put("issueOffice","");
                }else{
                    map.put("issueOffice",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_ISSUE_OFFICE).getString("words"));
                }
                if (wordsResult.isNull(BaiDuAiConstant.OCR_IDCARD_ISSUE_DATE)){
                    map.put("issueDate","");
                }else{
                    map.put("issueDate",wordsResult.getJSONObject(BaiDuAiConstant.OCR_IDCARD_ISSUE_DATE).getString("words"));
                }
                success=true;
            }else{
                message=thirdResult.getMessage();
            }
        }catch (Exception e){
            message=e.getLocalizedMessage();
        }
        return new BaseResult(success,message,map);
    }
}
