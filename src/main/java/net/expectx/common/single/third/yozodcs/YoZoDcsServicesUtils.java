package net.expectx.common.single.third.yozodcs;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.HttpUtils;
import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 */
public class YoZoDcsServicesUtils {
    public static BaseResult onlineFile(String url, Integer convertType) {
        Map<String, String> queries = new HashMap<>(2);
        Map<String, String> headers = new HashMap<>(2);
        Map<String, String> body = new HashMap<>(2);
        try {
            body.put("downloadUrl", url);
            body.put("convertType", String.valueOf(convertType));
            HttpResponse response = HttpUtils.doPost(YoZoDcsConstant.YOZODCS_SERVER_HOST, YoZoDcsConstant.ONLINE_FILE, YoZoDcsConstant.HTTP_REQUEST_TYPE_POST, headers, queries, body);
            return YoZoDcsConstant.analysisHttpResponse(response);
        } catch (Exception e) {
            return new BaseResult(false, e.getLocalizedMessage(), null);

        }
    }
    public static BaseResult onlineFileUpload(String filePath, Integer convertType) {
        try {
            String uploadResult=HttpUtils.doUploadFile(YoZoDcsConstant.YOZODCS_SERVER_HOST,YoZoDcsConstant.UPLOAD_FILE,filePath);
            if (uploadResult!=null){
                JSONObject uploadJson=JSONObject.parseObject(uploadResult);
                if (uploadJson.getInteger(YoZoDcsConstant.A).equals(YoZoDcsConstant.SUCCESS_STATUS)){
                    return onlineFileInputDirUpload(uploadJson.getString(YoZoDcsConstant.BASIC_IDENTIFICATION_DATA),convertType);
                }else{
                    return new BaseResult(false, uploadJson.getString(YoZoDcsConstant.BASIC_IDENTIFICATION_MESSAGE), null);
                }
            }else{
                return new BaseResult(false, "文件上传失败", null);
            }

        }catch (JSONException e){
            return new BaseResult(false,"文件上传服务器错误", null);
        }catch (Exception e) {
            return new BaseResult(false, e.getLocalizedMessage(), null);
        }
    }
    public static BaseResult onlineFileInputDirUpload(String inputDir, Integer convertType) {
        Map<String, String> queries = new HashMap<>(2);
        Map<String, String> headers = new HashMap<>(2);
        Map<String, String> body = new HashMap<>(2);
        try {
            System.out.println(inputDir);
            body.put("inputDir",inputDir);
            body.put("convertType", String.valueOf(convertType));
            HttpResponse response = HttpUtils.doPost(YoZoDcsConstant.YOZODCS_SERVER_HOST, YoZoDcsConstant.CONVERT, YoZoDcsConstant.HTTP_REQUEST_TYPE_POST, headers, queries, body);
            return YoZoDcsConstant.analysisHttpResponse(response);
        }catch (Exception e) {
            return new BaseResult(false, e.getLocalizedMessage(), null);
        }
    }
}
