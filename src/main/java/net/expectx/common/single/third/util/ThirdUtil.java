package net.expectx.common.single.third.util;

import java.util.Random;

/**
 * @author lijian
 */
public class ThirdUtil {
    /**
     * 生成 num位的随机数
     * @param num
     * @return
     */
    public static int randomSmsCode(final int num) {
        Random rand = new Random();
        int first = rand.nextInt(10);
        if(first == 0) {
            first = 6;
        }
        String code = String.valueOf(first);
        for(int i = 1; i < num; i++) {
            code += rand.nextInt(10);
        }
        return Integer.parseInt(code);
    }
}
