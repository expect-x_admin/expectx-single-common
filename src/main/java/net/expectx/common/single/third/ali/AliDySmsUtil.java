package net.expectx.common.single.third.ali;


import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.google.common.collect.Maps;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.third.util.ThirdUtil;
import net.expectx.common.single.util.PropertiesFileUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 * 阿里云短信工具
 */
public class AliDySmsUtil {
    public static final String ALI_ACCESSKEY_ID = PropertiesFileUtil.getInstance("third-client-config").get("ALI_ACCESSKEY_ID");
    public static final String ALI_ACCESSKEY_SECRET = PropertiesFileUtil.getInstance("third-client-config").get("ALI_ACCESSKEY_SECRET");
    static final String PRODUCT = "Dysmsapi";
    static final String DOMAIN = "dysmsapi.aliyuncs.com";
    private static String  aliDySmsSignName;
    private static String aliDySmsTemplateCode;
    private static final  String OK="OK";


    public static void config(String signName,String templateCode ) {
        aliDySmsSignName=signName;
        aliDySmsTemplateCode = templateCode;
    }


    public static BaseResult sendSms(String mobile, Map<String,String>paramsMap){
        Map<String,Object>map=new HashMap<String, Object>(2);
        Boolean flag=false;
        String msg="";
        try {
            /**
             * 可自助调整超时时间
             */
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");

            /**
             * 初始化acsClient,暂不支持region化
             */
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", ALI_ACCESSKEY_ID, ALI_ACCESSKEY_SECRET);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", PRODUCT, DOMAIN);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            /**
             * 组装请求对象-具体描述见控制台-文档部分内容
             */
            SendSmsRequest request = new SendSmsRequest();
            //必填:待发送手机号
            request.setPhoneNumbers(mobile);
            //必填:短信签名-可在短信控制台中找到
            request.setSignName(aliDySmsSignName);
            //必填:短信模板-可在短信控制台中找到
            request.setTemplateCode(aliDySmsTemplateCode);
            if (null!=paramsMap){
                request.setTemplateParam(JSON.toJSONString(paramsMap));
            }
            SendSmsResponse response = acsClient.getAcsResponse(request);
            System.out.println("短信接口返回的数据----------------");
            System.out.println("Code=" + response.getCode());
            System.out.println("Message=" + response.getMessage());
            System.out.println("RequestId=" + response.getRequestId());
            if(response.getCode() != null && OK.equals( response.getCode())) {
                QuerySendDetailsResponse querySendDetailsResponse = querySendDetails(response.getBizId());
                System.out.println("短信明细查询接口返回数据----------------");
                System.out.println("Code=" + querySendDetailsResponse.getCode());
                System.out.println("Message=" + querySendDetailsResponse.getMessage());
                flag=true;
                msg="短信发送成功";
            }else{
                msg= "短信发送失败:"+response.getMessage();
            }
        }catch (Exception e){
            msg="SmsUtil发生错误:"+e.getLocalizedMessage();
        }

        return  new BaseResult(flag,msg,null);
    }

    public static QuerySendDetailsResponse querySendDetails(String bizId) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", ALI_ACCESSKEY_ID, ALI_ACCESSKEY_SECRET);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", PRODUCT, DOMAIN);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber("15000000000");
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);

        //hint 此处可能会抛出异常，注意catch
        QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);

        return querySendDetailsResponse;
    }

    public static void main(String[] args) throws ClientException, InterruptedException {

        //发短信
        Map<String,String>paramsMap= Maps.newHashMapWithExpectedSize(7);
        int code = ThirdUtil.randomSmsCode(6);
        paramsMap.put("code","123421");
        config("智舰工作室","SMS_147418172");
        System.out.println(sendSms("18363003321",paramsMap));




    }
}
