package net.expectx.common.single.third.netease.im;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 网易云通信服务工具类
 * @author lijian
 */
public class NeteaseImServicesUtils {
    private static final Integer ONE_HUNDRED=100;
    private static final Integer TWO=2;

    /**
     * 创建网易云通信ID
     * @param accid         必填   网易云通信ID，最大长度32字符，必须保证一个 APP内唯一（只允许字母、数字、半角下划线_、  @、半角点以及半角-组成，不区分大小写， 会统一小写处理，请注意以此接口返回结果中的accid为准）
     * @param name          必填   网易云通信ID昵称，最大长度64字符，用来PUSH推送时显示的昵称
     * @param password      必填   网易云通信ID可以指定登录token值，最大长度128字符， 并更新，如果未指定，会自动生成token，并在创建成功后返回
     * @param icon          选填   网易云通信ID头像URL，第三方可选填，最大长度1024
     * @param sign          选填   用户签名，最大长度256字符
     * @param email         选填   用户email，最大长度64字符
     * @param birth         选填   用户生日，最大长度16字符
     * @param mobile        选填   用户mobile，最大长度32字符
     * @param gender        选填   用户性别，0表示未知(默认)，1表示男，2女表示女
     * @param props         选填   json属性，第三方可选填，最大长度1024字符
     * @param ex            选填   用户名片扩展字段，最大长度1024字符，用户可自行扩展，建议封装成JSON字符串
     * @return
     */
    public static BaseResult createUser(String accid, String name, String password, String icon, String sign, String email, String birth, String mobile, Integer gender, String props, String ex){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("accid",accid);
        querys.put("name",name);
        querys.put("token",password);
        querys.put("icon",icon);
        querys.put("sign",sign);
        querys.put("email",email);
        querys.put("birth",birth);
        querys.put("mobile",mobile);
        if (null!=gender){
            querys.put("gender",String.valueOf(gender));
        }else{
            querys.put("gender",String.valueOf(0));
        }

        if (StringUtils.isNotEmpty(props)){
            querys.put("props",props);
        }
        if (StringUtils.isNotEmpty(ex)){
            querys.put("ex",ex);
        }
        try{
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.USER_CREATE,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return  NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }

    /**
     * 创建群
     * @param teamName           必填   群名称，最大长度64字符
     * @param teamOwner          必填   群主用户帐号，最大长度32字符
     * @param members            必填   群组成员 list集合，一次最多拉200个成员
     * @param msg                必填   邀请发送的文字，最大长度150字符
     * @param magree             必填   管理后台建群时，0 不需要被邀请人同意加入群，1 需要被邀请人同意才可以加入群
     * @param joinmode           必填   群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入
     * @param teamAnnouncement   选填   群公告，最大长度1024字符
     * @param teamIntro          选填   群描述，最大长度512字符
     * @param teamIcon           选填   群头像，最大长度1024字符
     * @param custom             选填   自定义高级群扩展属性，第三方可以跟据此属性自定义扩展自己的群属性。（建议为json）,最大长度1024字符
     * @param beinvitemode       选填   被邀请人同意方式，0-需要同意(默认),1-不需要同意
     * @param invitemode         选填   谁可以邀请他人入群，0-管理员(默认),1-所有人
     * @param uptinfomode        选填   谁可以修改群资料，0-管理员(默认),1-所有人
     * @param upcustommode       选填   谁可以更新群自定义属性，0-管理员(默认),1-所有人
     * @return
     */

    public static  BaseResult createTeam(String teamName, String teamOwner, List <String> members,String msg,Integer magree,Integer joinmode,String teamAnnouncement,String teamIntro,String teamIcon,String custom,Integer beinvitemode,Integer invitemode,Integer uptinfomode,Integer upcustommode ){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tname",teamName);
        querys.put("owner",teamOwner);
        querys.put("members", JSON.toJSONString(members));
        querys.put("msg",msg);
        querys.put("magree",String.valueOf(magree));
        querys.put("joinmode",String.valueOf(joinmode));
        if (StringUtils.isNotEmpty(teamAnnouncement)){
            querys.put("announcement",teamAnnouncement);
        }
        if (StringUtils.isNotEmpty(teamIntro)){
            querys.put("intro",teamIntro);
        }
        if (StringUtils.isNotEmpty(custom)){
            querys.put("custom",custom);
        }
        if (StringUtils.isNotEmpty(teamIcon)){
            querys.put("icon",teamIcon);
        }
        if (null!=beinvitemode){
            querys.put("beinvitemode",String.valueOf(beinvitemode));
        }
        if (null!=invitemode){
            querys.put("invitemode",String.valueOf(invitemode));
        }
        if (null!=uptinfomode){
            querys.put("uptinfomode",String.valueOf(uptinfomode));
        }
        if (null!=upcustommode){
            querys.put("upcustommode",String.valueOf(upcustommode));
        }

        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_CTREATE,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return  NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 解散群
     * @param tid          必填  网易云通信服务器产生，群唯一标识，创建群时会返回，最大长度128字符
     * @param teamOwner    必填  群主用户帐号，最大长度32字符
     * @return
     */
    public static BaseResult removeTeam(Long tid,String teamOwner){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",teamOwner);
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_REMOVE,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 编辑群资料
     * @param tid                必填  网易云通信服务器产生，群唯一标识，创建群时会返回，最大长度128字符
     * @param teamName           必填   群名称，最大长度64字符
     * @param teamOwner          必填   群主用户帐号，最大长度32字符
     * @param teamAnnouncement   选填   群公告，最大长度1024字符
     * @param teamIntro          选填   群描述，最大长度512字符
     * @param joinmode           必填   群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入
     * @param teamIcon           选填   群头像，最大长度1024字符
     * @param custom             选填   自定义高级群扩展属性，第三方可以跟据此属性自定义扩展自己的群属性。（建议为json）,最大长度1024字符
     * @param beinvitemode       选填   被邀请人同意方式，0-需要同意(默认),1-不需要同意
     * @param invitemode         选填   谁可以邀请他人入群，0-管理员(默认),1-所有人
     * @param uptinfomode        选填   谁可以修改群资料，0-管理员(默认),1-所有人
     * @param upcustommode       选填   谁可以更新群自定义属性，0-管理员(默认),1-所有人
     * @return
     */
    public static BaseResult updateTeam(Long tid,String teamName,String teamOwner,String teamAnnouncement,String teamIntro,Integer joinmode,String custom,String teamIcon,Integer beinvitemode,Integer invitemode,Integer uptinfomode,Integer upcustommode){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",tid.toString());
        querys.put("tname",teamName);
        querys.put("owner",teamOwner);
        querys.put("joinmode",String.valueOf(joinmode));
        if (StringUtils.isNotEmpty(teamAnnouncement)){
            querys.put("announcement",teamAnnouncement);
        }
        if (StringUtils.isNotEmpty(teamIntro)){
            querys.put("intro",teamIntro);
        }
        if (StringUtils.isNotEmpty(custom)){
            querys.put("custom",custom);
        }
        if (StringUtils.isNotEmpty(teamIcon)){
            querys.put("icon",teamIcon);
        }
        if (null!=beinvitemode){
            querys.put("beinvitemode",String.valueOf(beinvitemode));
        }
        if (null!=invitemode){
            querys.put("invitemode",String.valueOf(invitemode));
        }
        if (null!=uptinfomode){
            querys.put("uptinfomode",String.valueOf(uptinfomode));
        }
        if (null!=upcustommode){
            querys.put("upcustommode",String.valueOf(upcustommode));
        }

        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_UPDATE,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 拉人入群
     * @param tid          必填  网易云通信服务器产生，群唯一标识，创建群时会返回
     * @param teamOwner    必填   群主用户帐号，最大长度32字符
     * @param members      必填   群组成员 list集合，一次最多拉200个成员
     * @param magree       必填   管理后台建群时，0 不需要被邀请人同意加入群，1 需要被邀请人同意才可以加入群
     * @param msg          必填   邀请发送的文字，最大长度150字符
     * @param attach       选填   自定义扩展字段，最大长度512
     * @return
     */
    public static  BaseResult addTeam(Long tid,String teamOwner,List members,Integer magree,String msg,String attach ){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",teamOwner);
        querys.put("members", JSON.toJSONString(members));
        querys.put("magree",String.valueOf(magree));
        querys.put("msg",msg);
        if (StringUtils.isNotEmpty(attach)){
            querys.put("attach",attach);
        }
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_ADD,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 踢人出群
     * @param tid           必填   网易云通信服务器产生，群唯一标识，创建群时会返回，最大长度128字符
     * @param teamOwner     必填   群主的accid，用户帐号，最大长度32字符
     * @param member        必填   被移除人的accid，用户账号，最大长度字符
     * @param attach        选填   自定义扩展字段，最大长度512
     * @return
     */
    public static  BaseResult kickTeam(Long tid,String teamOwner,String member,String attach ){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",teamOwner);
        querys.put("member",member);
        if (StringUtils.isNotEmpty(attach)){
            querys.put("attach",attach);
        }
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_KICK,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 群信息与成员列表查询
     * @param tids   必填  群id列表，list集合
     * @param ope    必填  1表示带上群成员列表，0表示不带群成员列表（默认），只返回群信息
     * @return
     */
    public static  BaseResult queryTeam(List tids,Integer ope){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tids",JSON.toJSONString(tids));
        if (null==ope){
            querys.put("ope",String.valueOf(0));
        }else{
            querys.put("ope",String.valueOf(ope));
        }
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_QUERY,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 获取某用户所加入的群信息
     * @param accId  要查询用户的accId
     * @return
     */
    public static  BaseResult joinTeams(String accId){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("accid",accId);
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response =HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_JOIN_TEAMS,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 获取群组详细信息
     * @param tid   群组ID
     * @return
     */
    public static BaseResult teamQueryDetail(Long tid){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        try{
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_QUERY_DETAIL,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 移交群主
     * @param tid           网易云通信服务器产生，群唯一标识，创建群时会返回
     * @param owner         群主用户帐号
     * @param newOwner      新群主帐号
     * @param leave         1:群主解除群主后离开群，2：群主解除群主后成为普通成员(默认)。其它414
     * @return
     */
    public static  BaseResult teamChangeOwner(Long tid,String owner,String newOwner,Integer leave){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",String.valueOf(tid));
        querys.put("newowner",String.valueOf(newOwner));
        if (null==leave){
            querys.put("leave",String.valueOf(2));
        }else{
            querys.put("leave",String.valueOf(leave));
        }
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_CHANGE_OWNER,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 任命管理员
     * @param tid           网易云通信服务器产生，群唯一标识，创建群时会返回
     * @param owner         群主用户帐号
     * @param members       ["aaa","bbb"](JSONArray对应的accid，如果解析出错会报414)，长度最大1024字符（一次添加最多10个管理员）
     * @return
     */
    public static  BaseResult teamAddManager(Long tid,String owner,List members){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",String.valueOf(tid));
        querys.put("members",JSON.toJSONString(members));
        Map<String,Object>resultMap=new HashMap<>(2);
        try{
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_ADD_MANAGER,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return  NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }
    /**
     * 移除管理员
     * @param tid           网易云通信服务器产生，群唯一标识，创建群时会返回
     * @param owner         群主用户帐号
     * @param members       ["aaa","bbb"](JSONArray对应的accid，如果解析出错会报414)，长度最大1024字符（一次添加最多10个管理员）
     * @return
     */
    public static  BaseResult teamRemoveManager(Long tid,String owner,List members){
        Map<String, String> querys = new HashMap<>(2);
        querys.put("tid",String.valueOf(tid));
        querys.put("owner",String.valueOf(tid));
        querys.put("members",JSON.toJSONString(members));
        try{
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.TEAM_REMOVE_MANAGER,NeteaseImConstant.HTTP_REQUEST_TYPE_POST,NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        }catch (Exception e){
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 获取用户名片
     * @param accIds  账户ID
     * @return
     */
    public static BaseResult userInfos(List<String> accIds) {
        Map<String, String> querys = new HashMap<>(16);
        querys.put("accids",JSON.toJSONString(accIds));
        try {
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.USER_INFOS,
                    NeteaseImConstant.HTTP_REQUEST_TYPE_POST, NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        } catch (Exception e) {
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }

    /**
     * 发送消息
     * @param from   发送消息人
     * @param to     接收消息人   ope==0是表示accid即用户id，ope==1表示tid即群id
     * @param ope    操作 0：点对点个人消息，1：群消息（高级群），其他返回414
     * @param type   消息类型   0 表示文本消息,
     *                          1 表示图片，
     *                          2 表示语音，
     *                          3 表示视频，
     *                          4 表示地理位置信息，
     *                          6 表示文件，
     *                          100 自定义消息类型（特别注意，对于未对接易盾反垃圾功能的应用，该类型的消息不会提交反垃圾系统检测）
     * @param content  最大长度5000字符
     * @return
     */
    public static BaseResult sendMsg(String from,String to,Integer ope,Integer type,String content) {
        Map<String,String>msgMap= Maps.newHashMapWithExpectedSize(7);
        msgMap.put("msg",content);
        Map<String, String> querys = new HashMap<>(16);
        querys.put("from",from);
        querys.put("to",to);
        querys.put("ope",String.valueOf(ope));
        querys.put("type",String.valueOf(type));
        querys.put("body",JSON.toJSONString(msgMap));
        try {
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.MSG_SEND_MSG,
                    NeteaseImConstant.HTTP_REQUEST_TYPE_POST, NeteaseImConstant.commonHeaders(),querys,"");
            return  NeteaseImConstant.analysisHttpResponse(response);
        } catch (Exception e) {
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }
    /**
     * 发送消息
     * @param from   发送消息人
     * @param to
     * @param hours  查看几小时的消息
     * @param limit  本次查询的消息条数上限(最多100条),小于等于0，或者大于100
     * @param reverse 1按时间正序排列，2按时间降序排列。.默认是按降序排列
     * @return
     */
    public static BaseResult querySessionMsg(String from,String to,Integer hours,Integer limit,Integer reverse) {
        Map<String, String> querys = new HashMap<>(16);
        if (limit<=0 && limit>ONE_HUNDRED){
            limit=10;
        }
        if (reverse!=1 && reverse.equals(TWO)){
            reverse=2;
        }
        Long nowTime=System.currentTimeMillis();
        Long beginTime=nowTime-(hours*60*60*1000);
        querys.put("from",from);
        querys.put("to",to);
        querys.put("begintime",String.valueOf(beginTime));
        querys.put("endtime",String.valueOf(nowTime));
        querys.put("limit",String.valueOf(limit));
        querys.put("reverse",String.valueOf(reverse));
        try {
            HttpResponse response = HttpUtils.doPost(NeteaseImConstant.NIM_SERVER_HOST,NeteaseImConstant.HISTORY_QUERY_SESSION_MSG,
                    NeteaseImConstant.HTTP_REQUEST_TYPE_POST, NeteaseImConstant.commonHeaders(),querys,"");
            return NeteaseImConstant.analysisHttpResponse(response);
        } catch (Exception e) {
            return new BaseResult(false,e.getLocalizedMessage(),null);

        }
    }




}
