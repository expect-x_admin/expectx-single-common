package net.expectx.common.single.third.dingtalk;

import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseConstants;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;
/**
 * @author lijian
 */
public class DingTalkConstant extends BaseConstants {
    public static final String APP_KEY = PropertiesFileUtil.getInstance("third-client-config").get("DING_TALK_APP_KEY");
    public static final String APP_SECRET = PropertiesFileUtil.getInstance("third-client-config").get("DING_TALK_APP_SECRET");
    public static final Integer SUCCESS_STATUS=0;
    private static final String A="errcode";

    /**
     * Dingtalk Server服务主机
     */
    public static final String DING_TALK_SERVER_HOST="https://oapi.dingtalk.com/";

    /**
     * 获得accessToken
     */
    public static final String GET_TOKEN="gettoken";

    /**
     * 获得部门列表
     */
    public static final String GET_DEPARTMENT_LIST="department/list";

    /**
     * 获取部门用户
     */
    public static final String GET_USER_LIST="user/simplelist";
    /**
     * 获取部门用户详情
     */
    public static final String GET_USER_LIST_BY_PAGE="user/listbypage";
    /**
     * 获取角色列表
     */
    public static final String GET_ROLE_LIST="topapi/role/list";
    /**
     * 获取角色下的员工列表
     */
    public static final String GET_ROLE_USER_LIST="topapi/role/simplelist";

    /**
     * 获取用户日志数据
     */
    public static final String GET_REPORT_LIST="topapi/report/list";

    /**
     * 公共请求头
     * @return
     */
    public static Map<String,String> commonHeaders(){
        Map<String, String> headers = new HashMap<>(2);
        return headers;
    }

    public static String getCodeStatusMessage(Integer code){
        Map<Integer, String> codeStatus = new HashMap<>(2);
        codeStatus.put(0,"请求成功");
        codeStatus.put(40001,"获取access_token时Secret错误,或者access_token无效");
        codeStatus.put(40068,"不合法的偏移量,偏移量必须大于0");
        codeStatus.put(60020,"访问ip不在白名单之中");




        return codeStatus.get(code);
    }
    public static BaseResult analysisHttpResponse(HttpResponse response) throws Exception{
        Boolean flag=false;
        String msg;
        JSONObject json=new JSONObject();
        if (response.getStatusLine().getStatusCode()== STATUS_OK){
            json=JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
            msg=getCodeStatusMessage(json.getInteger(A));
            if (SUCCESS_STATUS.equals(json.getInteger(A))){
                flag=true;
            }
        }else{
            msg=getCodeStatusMessage(response.getStatusLine().getStatusCode());
        }
        return new BaseResult(flag,msg,json);
    }
}
