package net.expectx.common.single.third.netease.im;

import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseConstants;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.third.api.ApiConstant;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 * 网易云信
 */
public class NeteaseImConstant extends BaseConstants {
    public static final String APP_KEY = PropertiesFileUtil.getInstance("third-client-config").get("NETEASE_IM_APP_KEY");
    public static final String APP_SECRET = PropertiesFileUtil.getInstance("third-client-config").get("NETEASE_IM_APP_SECRET") ;
    public static final Integer SUCCESS_STATUS=200;
    /**
     * NeteaseIM Server服务主机
     */
    public static final String NIM_SERVER_HOST="https://api.netease.im/nimserver";

    /**
     * 创建网易云通信ID
     */
    public static final String USER_CREATE="/user/create.action";
    /**
     * 获取用户名片
     */
    public static final String USER_INFOS = "/user/getUinfos.action";


    /**
     * 创建群
     */
    public static final String TEAM_CTREATE="/team/create.action";

    /**
     * 解散群
     */
    public static final String TEAM_REMOVE="/team/remove.action";

    /**
     * 编辑群资料
     */
    public static final String TEAM_UPDATE="/team/update.action";

    /**
     * 拉人入群
     */
    public static final String TEAM_ADD="/team/add.action";

    /**
     * 踢人出群
     */
    public static final String TEAM_KICK="/team/kick.action";

    /**
     * 群信息与成员列表查询
     */
    public static final String TEAM_QUERY="/team/query.action";
    /**
     * 获取某用户所加入的群信息
     */
    public static final String TEAM_JOIN_TEAMS="/team/joinTeams.action";
    /**
     * 获取群组详细信息
     */
    public static final String TEAM_QUERY_DETAIL="/team/queryDetail.action";

    /**
     * 移交群主
     */
    public static final String TEAM_CHANGE_OWNER="/team/changeOwner.action";
    /**
     * 任命管理员
     */
    public static final String TEAM_ADD_MANAGER="/team/addManager.action";
    /**
     * 移除管理员
     */
    public static final String TEAM_REMOVE_MANAGER="/team/removeManager.action";


    /**
     * 发送普通消息
     */
    public static final String MSG_SEND_MSG="/msg/sendMsg.action";


    /**
     * 单聊云端历史消息查询
     */
    public static final String HISTORY_QUERY_SESSION_MSG="/history/querySessionMsg.action";

    /**
     * 公共请求头
     * @return
     */
    public static Map<String,String>commonHeaders(){
        String nonce= NeteaseImUtils.getNonce();
        String curTime= NeteaseImUtils.getUTCTimestamp();
        String checkSum = NeteaseImUtils.getCheckSum(APP_SECRET,nonce,curTime);
        Map<String, String> headers = new HashMap<>(2);
        headers.put("APPKEY", APP_KEY);
        headers.put("CurTime",curTime);
        headers.put("CheckSum",checkSum);
        headers.put("Nonce",nonce);
        headers.put("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        return headers;
    }

    public static String getCodeStatusMessage(Integer code){
        Map<Integer, String> codeStatus = new HashMap<>(2);
        codeStatus.put(200,"操作成功");
        codeStatus.put(201,"客户端版本不对，需升级sdk");
        codeStatus.put(301,"被封禁");
        codeStatus.put(302,"用户名或密码错误");
        codeStatus.put(315,"IP限制");
        codeStatus.put(403,"非法操作或没有权限");
        codeStatus.put(404,"对象不存在");
        codeStatus.put(405,"参数长度过长");
        codeStatus.put(406,"对象只读");
        codeStatus.put(408,"客户端请求超时");
        codeStatus.put(413,"验证失败(短信服务)");
        codeStatus.put(414,"参数错误");
        codeStatus.put(415	,"客户端网络问题");
        codeStatus.put(416	,"频率控制");
        codeStatus.put(417	,"重复操作");
        codeStatus.put(418	,"通道不可用(短信服务)");
        codeStatus.put(419	,"数量超过上限");
        codeStatus.put(422	,"账号被禁用");
        codeStatus.put(431	,"HTTP重复请求");
        codeStatus.put(500	,"服务器内部错误");
        codeStatus.put(503	,"服务器繁忙");
        codeStatus.put(508	,"消息撤回时间超限");
        codeStatus.put(509	,"无效协议");
        codeStatus.put(514	,"服务不可用");
        codeStatus.put(998	,"解包错误");
        codeStatus.put(999	,"打包错误");
        codeStatus.put(801	,"群人数达到上限");
        codeStatus.put(802	,"没有权限");
        codeStatus.put(803	,"群不存在");
        codeStatus.put(804	,"用户不在群");
        codeStatus.put(805	,"群类型不匹配");
        codeStatus.put( 806	,"创建群数量达到限制");
        codeStatus.put(807	,"群成员状态错误");
        codeStatus.put(808	,"申请成功");
        codeStatus.put(809	,"已经在群内");
        codeStatus.put(810	,"邀请成功");
        codeStatus.put(9102,"通道失效");
        codeStatus.put(9103,"已经在他端对这个呼叫响应过了");
        codeStatus.put(11001,"通话不可达，对方离线状态");
        codeStatus.put(13001,"IM主连接状态异常");
        codeStatus.put(13002,"聊天室状态异常");
        codeStatus.put(13003,"账号在黑名单中,不允许进入聊天室");
        codeStatus.put(13004,"在禁言列表中,不允许发言");
        codeStatus.put(13005,"用户的聊天室昵称、头像或成员扩展字段被反垃圾");
        codeStatus.put(10431,"输入email不是邮箱");
        codeStatus.put(10432,"输入mobile不是手机号码");
        codeStatus.put(10433,"注册输入的两次密码不相同");
        codeStatus.put(10434,"企业不存在");
        codeStatus.put(10435,"登陆密码或帐号不对");
        codeStatus.put(10436,"app不存在");
        codeStatus.put(10437,"email已注册");
        codeStatus.put(10438,"手机号已注册");
        codeStatus.put(10441,"app名字已经存在");
        return codeStatus.get(code);
    }
    public static BaseResult analysisHttpResponse(HttpResponse response) throws Exception{
        Map<String,Object>resultMap=new HashMap<>(2);
        Boolean flag=false;
        String msg=null;
        JSONObject json=new JSONObject();
        if (response.getStatusLine().getStatusCode()==STATUS_OK){
            json=JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
            msg=getCodeStatusMessage(json.getInteger(ApiConstant.BASIC_IDENTIFICATION_CODE));
            if (SUCCESS_STATUS.equals(json.getInteger(ApiConstant.BASIC_IDENTIFICATION_CODE))){
                flag=true;
                resultMap.put(BaseConstants.BASIC_IDENTIFICATION_DATA,json);
            }
        }else{
            msg=getCodeStatusMessage(response.getStatusLine().getStatusCode());
        }
        resultMap.put("msg",msg);
        return  new BaseResult(flag,msg,json);
    }

}
