package net.expectx.common.single.third.baidu;

import com.baidu.aip.ocr.AipOcr;

/**
 * @author lijian
 * @desc AipOCR单利模式
 * @dateTime 6/5/2018 9:40 AM
 * @info 18363003321/974028417@qq.com
 */
public class AipOcrClientSingleton {

    private AipOcr client=null;
    private static AipOcrClientSingleton instance;

    private AipOcrClientSingleton(){

        client = new AipOcr(BaiDuAiConstant.APP_ID, BaiDuAiConstant.API_KEY, BaiDuAiConstant.SECRET_KEY);
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

    }

    public static synchronized AipOcrClientSingleton getInstance(){

        if(instance==null){
            instance=new AipOcrClientSingleton();
        }
        return instance;

    }

    public  synchronized final AipOcr getAipOcr(){

        return client;

    }

}
