package net.expectx.common.single.third.ali;


import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.google.common.collect.Maps;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.apache.commons.lang.StringUtils;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

/**
 * @author lijian
 * 阿里云储工具类
 */
public class AliOssUtil {

    /**
     * 阿里云存储切入点
     */
    private static String OSS_END_PORT= PropertiesFileUtil.getInstance("third-client-config").get("ALI_OSS_END_PORT");
    /**
     * 阿里云存储密钥
     */
    private static final String OSS_ACCESS_KEY_ID = PropertiesFileUtil.getInstance("third-client-config").get("ALI_OSS_ACCESS_KEY_ID");
    /**
     * 阿里云存储安全密钥
     */
    private static final String OSS_ACCESS_KEY_SECRET= PropertiesFileUtil.getInstance("third-client-config").get("ALI_OSS_ACCESS_KEY_SERCET");
    /**
     * 阿里云存储文件夹
     */
    private static final String OSS_BUCK_NAME= PropertiesFileUtil.getInstance("third-client-config").get("ALI_OSS_BUCK_NAME");
    public static BaseResult upload(String root, String catalog, String imageName, InputStream in){
        Map<String,Object>map= Maps.newHashMapWithExpectedSize(7);
        Boolean flag=false;
        String msg="";
        try {
            String fileUrl=null;
            OSSClient client = new OSSClient(OSS_END_PORT,  OSS_ACCESS_KEY_ID,OSS_ACCESS_KEY_SECRET);
            if (StringUtils.isNotEmpty(catalog)){
                fileUrl="http://"+OSS_BUCK_NAME+"."+OSS_END_PORT+"/"+root+"/"+catalog+"/"+imageName;
                client.putObject(OSS_BUCK_NAME, root+"/"+catalog+"/"+imageName, in);
            }else {
                fileUrl="http://"+OSS_BUCK_NAME+"."+OSS_END_PORT+"/"+root+"/"+imageName;
                client.putObject(OSS_BUCK_NAME, root+"/"+imageName, in);
            }
            flag=true;
            map.put("fileUrl",fileUrl);
            client.shutdown();
        }catch (Exception e){
            msg="SmsUtil发生错误:"+e.getLocalizedMessage();
            System.out.println("SmsUtil发生错误:"+e.getLocalizedMessage());
        }
        return  new BaseResult(flag,msg,map);
    }

    public static BaseResult uploadBase64(String root,String catalog, String imageName,String base64){
        Map<String,Object>map= Maps.newHashMapWithExpectedSize(7);
        Boolean flag=false;
        String msg="";
        try {
            String fileUrl=null;

            OSSClient client = new OSSClient(OSS_END_PORT,OSS_ACCESS_KEY_ID,OSS_ACCESS_KEY_SECRET);
            byte[] bytes = new BASE64Decoder().decodeBuffer(base64);
            InputStream inputStream = new ByteArrayInputStream(bytes);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(inputStream.available());
            if (StringUtils.isNotEmpty(catalog)){
                fileUrl="http://"+OSS_BUCK_NAME+"."+OSS_END_PORT+"/"+root+"/"+catalog+"/"+imageName;
                client.putObject(OSS_BUCK_NAME, root+"/"+catalog+"/"+imageName, inputStream);
            }else {
                fileUrl="http://"+OSS_BUCK_NAME+"."+OSS_END_PORT+"/"+root+"/"+imageName;
                client.putObject(OSS_BUCK_NAME, root+"/"+imageName, inputStream);
            }
           flag=true;
            map.put("fileUrl",fileUrl);
            client.shutdown();
        }catch (Exception e){
            msg="SmsUtil发生错误:"+e.getLocalizedMessage();
            System.out.println("SmsUtil发生错误:"+e.getLocalizedMessage());
        }

        return  new BaseResult(flag,msg,map);

    }


    public static void main(String[] args) throws Exception, InterruptedException {

        System.out.println(upload("ExpectX","user","1111.png",new FileInputStream("C:\\Users\\Administrator\\Desktop\\微信图片_20181228155003.jpg")));

    }
}
