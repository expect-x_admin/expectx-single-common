package net.expectx.common.single.third.api;


import net.expectx.common.single.base.BaseConstants;

/**
 * @author lijian
 */
public class ApiConstant extends BaseConstants {
    /**
     * 加密前缀
     */
    public static final String REDIS_KEY_ENCRYPTION_PREFIX = "ENCRYPTION_KEY";
    /**
     * TT基本信息服务主机
     */
    public static final String TI_TI_BASE_INFO_SERVER="http://127.0.0.1:6001";
    public static final String TI_TI_BASE_INFO_SERVER_HOST=TI_TI_BASE_INFO_SERVER+"/api/v1/";





    /**
     * 获得加密方式
     */
    public static final String SYSTEM_ENCRYPTION="encryption.action";

}
