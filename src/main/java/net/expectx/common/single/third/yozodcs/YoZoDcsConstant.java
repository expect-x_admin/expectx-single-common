package net.expectx.common.single.third.yozodcs;

import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseConstants;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 * 永中
 */
public class YoZoDcsConstant extends BaseConstants {
    private static final Logger LOGGER = Logger.getLogger(YoZoDcsConstant.class);

    public static final String APP_KEY = PropertiesFileUtil.getInstance("third-client-config").get("WEI_XIN_APP_KEY");
    public static final String APP_SECRET = PropertiesFileUtil.getInstance("third-client-config").get("WEI_XIN_APP_SECRET");
    public static final Integer SUCCESS_STATUS=0;
    public static final String A="result";

    /**
     * http://www.yozodcs.com/ Server服务主机
     */
    public static final String YOZODCS_SERVER_HOST="http://www.yozodcs.com/";

    /**
     * 获得accessToken
     */
    public static final String ONLINE_FILE="onlinefile";

    public static final String UPLOAD_FILE="testUpload";
    public static final String CONVERT="convert";
    public static String getCodeStatusMessage(Integer code){
        Map<Integer, String> codeStatus = new HashMap<>(2);
        codeStatus.put(0,"转换成功");
        codeStatus.put(40001,"获取access_token时Secret错误,或者access_token无效");
        codeStatus.put(40068,"不合法的偏移量,偏移量必须大于0");
        codeStatus.put(60020,"访问ip不在白名单之中");




        return codeStatus.get(code);
    }
    public static BaseResult analysisHttpResponse(HttpResponse response) throws Exception{
        Boolean flag=false;
        String msg;
        JSONObject json=new JSONObject();
        if (response.getStatusLine().getStatusCode()== STATUS_OK){
            json=JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
            msg=getCodeStatusMessage(json.getInteger(A));
            if (SUCCESS_STATUS.equals(json.getInteger(A))){
                flag=true;
            }
        }else{
            msg=getCodeStatusMessage(response.getStatusLine().getStatusCode());
        }
        return new BaseResult(flag,msg,json);
    }
}
