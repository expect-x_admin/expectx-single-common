package net.expectx.common.single.third.api;


import com.alibaba.fastjson.JSONObject;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 */
public class ApiServicesUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiServicesUtils.class);

    public static BaseResult systemEncryption(){
        Map<String,String>params=new HashMap<>(2);
        Map<String,String>header= ApiUtil.commonHeaders();
        try{
            HttpResponse response = HttpUtils.doGet(ApiConstant.TI_TI_BASE_INFO_SERVER_HOST, ApiConstant.SYSTEM_ENCRYPTION, ApiConstant.HTTP_REQUEST_TYPE_GET,header,params);
            return new BaseResult(true,null,JSONObject.parseObject(EntityUtils.toString(response.getEntity())));
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult(false,e.getLocalizedMessage(),null);
        }
    }



    public static void main(String [] args){


    }
}
