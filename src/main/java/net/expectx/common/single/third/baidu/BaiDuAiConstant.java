package net.expectx.common.single.third.baidu;

import net.expectx.common.single.base.BaseConstants;
import net.expectx.common.single.base.BaseResult;
import net.expectx.common.single.util.PropertiesFileUtil;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 百度AIchangliang
 * @author lijian
 */
public class BaiDuAiConstant extends BaseConstants {

    public static final String APP_ID = PropertiesFileUtil.getInstance("third-client-config").get("BAI_DU_AI_APP_ID");
    public static final String API_KEY = PropertiesFileUtil.getInstance("third-client-config").get("BAI_DU_AI_API_KEY");
    public static final String SECRET_KEY = PropertiesFileUtil.getInstance("third-client-config").get("BAI_DU_AI_SECRET_KEY");

    public static final String OCR_IDCARD_REAL_NAME="姓名";
    public static final String OCR_IDCARD_REAL_NATION="民族";
    public static final String OCR_IDCARD_ADDRESS="住址";
    public static final String OCR_IDCARD_ID="公民身份号码";
    public static final String OCR_IDCARD_BIRTHDAY="出生";
    public static final String OCR_IDCARD_SEX="性别";
    public static final String OCR_IDCARD_EXPIRATION_DATE="失效日期";
    public static final String OCR_IDCARD_ISSUE_OFFICE="签发机关";
    public static final String OCR_IDCARD_ISSUE_DATE="签发日期";
    private static final String ERROR_CODE="error_code";

    public static String getCodeStatusMessage(Integer code){

        Map<String, String> codeStatus = new HashMap<>(2);

        codeStatus.put("SDK102","读取图片文件错误");
        codeStatus.put("216015","模块关闭");
        codeStatus.put("216100","非法参数");
        codeStatus.put("216101","参数数量不够");
        codeStatus.put("216102","业务不支持");
        codeStatus.put("216103","参数太长");
        codeStatus.put("216110","APP ID不存在");
        codeStatus.put("216111","非法用户ID");
        codeStatus.put("216200","空的图片");
        codeStatus.put("216201","图片格式错误");
        codeStatus.put("216202","图片大小错误");
        codeStatus.put("216300","DB错误");
        codeStatus.put("216400","后端系统错误");
        codeStatus.put("216401","内部错误");
        codeStatus.put("216500","未知错误");
        codeStatus.put("216600","身份证的ID格式错误");
        codeStatus.put("216601","身份证的ID和名字不匹配");
        codeStatus.put("216630","识别错误");
        codeStatus.put("216631","识别银行卡错误（通常为检测不到银行卡）");
        codeStatus.put("216632","unknown error");
        codeStatus.put("216633","识别身份证错误（通常为检测不到身份证）");
        codeStatus.put("216634","检测错误");
        codeStatus.put("216635","获取mask图片错误");
        codeStatus.put("282000","业务逻辑层内部错误");
        codeStatus.put("282001","业务逻辑层后端服务错误");
        codeStatus.put("282100","图片压缩转码错误");

        return codeStatus.get(code);

    }

    public static BaseResult analysisJSON(JSONObject json) throws Exception{
        Boolean success=false;
        String msg=null;
        if (json.has("")){
            msg=getCodeStatusMessage(json.getInt(ERROR_CODE));
        } else {
            success = true;
        }
        return new BaseResult(success,msg,json);

    }

}
